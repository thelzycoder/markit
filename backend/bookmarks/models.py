from django.db import models
from appuser.models import AppUser, Base

class Category(Base):
    """
    :field: name: Name of the category
    :field: created_by: User the bookmark was created_by
    :field: created_at: What time the model was created at
    :field: marked_for_deletion: If this was marked for deletion
    """
    name = models.CharField(max_length=127)
    created_by = models.ForeignKey(AppUser, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name


class Bookmark(Base):
    """
    Model to save the bookmark
    :field: title: Title of the bookmark
    :field: link: Link of the bookmark
    :field: notes: Notes added by the user
    :field: created_by: User the bookmark was created_by
    :field: categories: The different categories the bookmark is associated to
    """
    title = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    notes = models.TextField()
    created_by = models.ForeignKey(AppUser, on_delete=models.SET_NULL, null=True, related_name='created_by')
    categories = models.ManyToManyField(Category)
    liked_by = models.ManyToManyField(AppUser, related_name='liked_by')

    def __str__(self):
        return self.title


class Comment(Base):
    """
    :field: comment_text: Comment
    :field: written_by: Comment was written by him
    :field: for_bookmark: Comment for bookmark
    """
    comment_text = models.CharField(max_length=511)
    written_by = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    for_bookmark = models.ForeignKey(Bookmark, on_delete=models.CASCADE)

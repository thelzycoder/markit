from bookmarks.views import BookmarkViewset
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'bookmarks',BookmarkViewset, basename='bookmark')

urlpatterns = router.urls

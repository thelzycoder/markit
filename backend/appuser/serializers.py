from builtins import object

from .models import AppUser
from rest_framework import serializers

class AppUserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = AppUser
        exclude = ('marked_for_deletion', 'created_at')

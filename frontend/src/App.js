import React, { Component } from 'react';
import './App.css';
import './markit.css';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import ProfilePosts from './components/bodycomponents/ProfilePosts';
import MainBody from './components/bodycomponents/MainBody';

class App extends Component {
  state = {
    categories: [
        {id: 1, name: "Technology", created_by: 1},
        {id: 2, name: "Startups", created_by: 2},
        {id: 3, name: "Design", created_by: 1}
    ],
    bookmarks: [
        {
            id: 1,
            title: "The end of app stores is rapidly approaching",
            link: "https://onezero.medium.com/the-end-of-app-stores-is-rapidly-approaching-b972da395097",
            notes: "Interesting bookmark 1",
            created_by: 1,
            categories: [1],
            liked_by: [1,2,3],
            created_at: "September 24, 2019"
        },
        {
            id: 2,
            title: "Overcoming the limitations in retail curbside",
            link: "https://medium.com/@sriramworkdegree/overcoming-the-limitation-in-retail-curbside-pickup-using-machine-learning-3bde7e05168b",
            notes: "Interesting bookmark 2",
            created_by: 2,
            categories: [1, 2],
            liked_by: [2,3],
            created_at: "September 24, 2019"
        },
        {
            id: 3,
            title: "Hiring and keeping top talent on a startup budget",
            link: "https://medium.com/@jproco/hiring-and-keeping-top-talent-on-a-startup-budget-2af0c71ddc10",
            notes: "Interesting bookmark fun",
            created_by: 1,
            categories: [2],
            liked_by: [4],
            created_at: "September 24, 2019"
        },
        {
            id: 4,
            title: "If you had 1000 customers will you be ready",
            link: "https://medium.com/swlh/if-you-had-1000-customers-ready-to-buy-right-now-would-you-be-ready-2ddd24d0578e",
            notes: "Interesting bookmark 1",
            created_by: 3,
            categories: [2],
            liked_by: [2,4],
            created_at: "September 24, 2019"
        },
        {
            id: 5,
            title: "The 5ws of rapid prototype",
            link: "https://medium.com/better-programming/the-5-ws-of-the-rapid-prototype-8e7614f82d29",
            notes: "Interesting rapid prototype",
            created_by: 4,
            categories: [3],
            liked_by: [1,2],
            created_at: "September 24, 2019"
        },
        {
            id: 6,
            title: "The benefits of interactive prototypes for ux showcases",
            link: "https://medium.com/@alessandro.arbizzani/the-benefits-of-interactive-prototypes-for-ux-showcases-65afd04fef50",
            notes: "Interesting bookmark 1",
            created_by: 2,
            categories: [2,3],
            liked_by: [1,2,3],
            created_at: "September 24, 2019"
        },
    ],
    comments: [
        {
            id: 1,
            comment_text: "Comment 1",
            written_by: 1,
            for_bookmark: 2
        },
        {
            id: 2,
            comment_text: "Comment 2",
            written_by: 3,
            for_bookmark: 1
        },
        {
            id: 3,
            comment_text: "Comment 3",
            written_by: 2,
            for_bookmark: 1
        },
        {
            id: 4,
            comment_text: "Comment 4",
            written_by: 4,
            for_bookmark: 1
        }
    ],
    appUsers: [
        {
            id: 1,
            name: "Mayank",
            email: "mayank@mayank.com"
        },
        {
            id: 2,
            name: "Abhinav",
            email: "abhinav@abhinav.com"
        },
        {
            id: 3,
            name: "Aharsh",
            email: "aharsh@aharsh.com"
        },
        {
            id: 4,
            name: "Rahul",
            email: "rahul@rahul.com"
        }
    ],
    utils: {
        logged_in: false,
        user_id: null,
    }
  };
  is_logged_in = () => {
    return this.state.utils.logged_in;
  };
  get_bookmarks_list = () => {
    if (this.state.utils.logged_in) {
      return this.state.bookmarks.filter(
          bookmark => bookmark.created_by === this.state.utils.user_id)
    }
    else {
        return this.state.bookmarks;
    }
  };
  get_body_tag = () => {
    if (this.state.utils.logged_in) {
        return (<ProfilePosts bookmarks={this.get_bookmarks_list()} appUsers={this.state.appUsers} />)
    }
    else {
        return (<MainBody bookmarks={this.get_bookmarks_list()} appUsers={this.state.appUsers} />)
    }
  }
  render () {
    return (
      <React.Fragment>
        <Navbar utils={this.state.utils} appUser={this.state.appUsers[this.state.utils.user_id-1]} />
        {this.get_body_tag}
        <Footer />
      </React.Fragment>
      )
  }
}

export default App;

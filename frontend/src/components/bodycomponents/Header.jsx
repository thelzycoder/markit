import React, { Component } from "react"

class Header extends Component {

    state = {};
    render () {
        var bgImg = require('../../images/home-bg.jpg')
        console.log(bgImg)
        return (
            <header className="masthead" style={{ backgroundImage: 'url(' + bgImg + ')' }}>
              <div className="overlay"></div>
              <div className="container">
                <div className="row">
                  <div className="col-lg-8 col-md-10 mx-auto">
                    <div className="site-heading">
                      <h1>MarkIt</h1>
                      <span className="subheading">Create, sort, group your bookmarks</span>
                    </div>
                  </div>
                </div>
              </div>
            </header>
            )
    }
}

export default Header;

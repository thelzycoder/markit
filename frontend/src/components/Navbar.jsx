import React, { Component } from "react";
import { Link } from "react-router";

class Navbar extends Component {
    state = {
      utils : this.props.utils,
      appUser: this.props.appUser
    };
    checkLoggedIn = () => {
      if (this.state.utils.logged_in) {
        return (
                    <li className="nav-item dropdown">
                      <a className="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="www.google.com">{this.state.appUser.name}<span className="caret"></span></a>
                      <ul className="dropdown-menu">
                        <li className="nav-item nav-dropdown"><a href="profile.html">Profile</a></li>
                        <li className="nav-item nav-dropdown"><a href="www.google.com">Settings</a></li>
                        <li className="nav-item nav-dropdown"><a href="www.google.com">Sign Out</a></li>
                      </ul>
                    </li>)
      }
      else {
        return (
            <li className="nav-item">
              <a className="btn btn-primary" href="www.google.com">Sign In</a>
            </li>
            )
      }
    }
    render () {
        return (
            <nav className="navbar navbar-expand-lg navbar-light" id="mainNav">
              <div className="container">
                <a className="navbar-brand" href="index.html">MarkIt</a>
                <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="www.google.comnavbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i className="fas fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <form className="navbar-form" role="search">
                      <div className="input-group">
                          <input type="text" className="form-control" placeholder="Search" name="q" />
                          <div className="input-group-btn">
                              <button className="btn btn-default search-btn" type="submit"><i className="glyphicon glyphicon-search"></i></button>
                          </div>
                      </div>
                      </form>
                    </li>
                    {this.checkLoggedIn()}
                  </ul>
                </div>
              </div>
            </nav>
            )
    }
}

export default Navbar;

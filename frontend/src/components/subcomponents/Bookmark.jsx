import React, { Component } from "react"

class Bookmark extends Component {
    state = {
      bookmark: this.props.bookmark,
      appUser: this.props.appUser
    };
    render () {
        return (
            <React.Fragment>
              <div className="post-preview">
                <h3 className="post-subtitle">
                  {this.state.bookmark.title}
                </h3>
                <p className="post-meta">Posted by
                  <a href="www.google.com">{this.state.appUser.name}</a>
                    on {this.state.bookmark.created_at}</p>
                <a href={this.state.bookmark.link} target="_blank" rel="noopener noreferrer">Visit</a>
              </div>
              <hr />
            </React.Fragment>
         );
    }
}

export default Bookmark;
